<?php if (!defined('FLUX_ROOT')) exit; ?>
						</div> <!-- containerRight -->
					</div> <!-- container -->
					<div id="containerBottom"></div>
					<div class="clear"></div>
				</div>
				<div id="footer">
					<div class="footerNav">
						<ul>
							<li><a href="<?php echo $this->url('main'); ?>">Home</a></li>
							<li><a href="<?php echo $EADev['forum']; ?>">Forum</a></li>
							<li><a href="<?php echo $this->url('main','download'); ?>">Download</a></li>
							<li><a href="<?php echo $this->url('account','create'); ?>">Register</a></li>
							<li><a href="<?php echo $this->url('donate'); ?>">Donate</a></li>
							<li><a href="<?php echo $this->url('vote'); ?>">Vote</a></li>
							<li><a href="<?php echo $EADev['rms']; ?>">Review</a></li>
						</ul>
						<div class="credits">
							<a href="http://ea-dev.com" target="_blank"><img src="<?php echo $this->themePath('img/rahul.png'); ?>" alt="Rahul Dev" title="EADev"></a>
							<a href="http://fahad.ea-dev.com" target="_blank"><img src="<?php echo $this->themePath('img/fahad.png'); ?>" alt="Rahul Dev" title="BrightiX"></a>
						</div>
					</div>
					<div class="copyrights">
						Copyright (C) 2013 <b>YOUR Ragnarok Online</b><br/>
						All other copyrights and trademarks are property of Gravity, and their respective owners.
					</div>
				</div>
			</div> <!-- main -->
		</div> <!-- wrapper -->
	</body>
</html>

<?php if (!defined('FLUX_ROOT')) exit; $title = null; ?>
<?php
	$submitted = $params->get('searchDatabase');
	if ( isset( $submitted ) ) {
		$searchfor = $params->get('searchfor');

		$name = $params->get('name');

		if( $searchfor == "monster" ) {
			$this->redirect($this->url('monster','index&name='. $name));
		} else {
			$this->redirect($this->url('item','index&name='. $name));
		}
	}

?>